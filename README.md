# Apache Storm environment
This project outlines the steps for getting a Storm cluster up and running. It uses 
[VirtualBox](https://www.virtualbox.org/), [Vagrant](https://www.vagrantup.com/docs/getting-started/) 
and [Ansible](https://www.ansible.com/get-started/). Clone repository into your local machine and 
run **vagrant up**.  

More details on [Setting up a Storm Cluster using Vagrant and Ansible page](http://notes.vault7.net/apache/storm/setting-up-storm-cluster-using-vagrant-and-ansible/).
